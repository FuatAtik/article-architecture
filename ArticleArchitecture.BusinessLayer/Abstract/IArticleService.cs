﻿using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Abstract
{
    public interface IArticleService
    {
        IQueryable<Article> GetArticles();
        Article GetArticlesById(int Id);
        IQueryable<Article> GetArticlesByCategoryId(int Id);
        int Insert(Article article);
        int Update(Article article);
        int Delete(Article article);
    }
}
