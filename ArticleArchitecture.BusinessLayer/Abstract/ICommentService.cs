﻿using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Abstract
{
    public interface ICommentService
    {
        IQueryable<Comment> GetAllComment();
        Comment GetCommentById(int id);
        int Update(Comment comment);
        int Delete(Comment comment);
        int Insert(Comment comment);
    }
}
