﻿using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Abstract
{
    public interface ILikeService
    {
        IQueryable<Liked> GetAllLikeArticle();
        List<Liked> GetAllLike();
        int Delete(Liked liked);
        int Insert(Liked liked);

    }
}
