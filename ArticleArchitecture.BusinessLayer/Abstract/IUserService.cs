﻿using ArticleArchitecture.BusinessLayer.Results;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Abstract
{
    public interface IUserService
    {
        BusinessLayerResult<User> Register(RegisterViewModel registerViewModel);
        BusinessLayerResult<User> Login(LoginViewModel loginViewModel);
        BusinessLayerResult<User> ActivateUser(Guid activateId);
        BusinessLayerResult<User> GetUserById(int id);
        BusinessLayerResult<User> UpdateProfile(User data);
        BusinessLayerResult<User> RemoveUserById(int id);
        BusinessLayerResult<User> Insert(User data);
        BusinessLayerResult<User> Update(User data);
        List<User> GetAllUser();
    }
}
