﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.DataAccessLayer.EntityFramework;
using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Concrete
{
    public class ArticleManager : IArticleService
    {
        public Repository<Article> _articleRepository;

        public ArticleManager(Repository<Article> articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IQueryable<Article> GetArticlesByCategoryId(int Id)
        {
            return _articleRepository.ListQueryable().Where(
                 x => x.IsDraft == false && x.CategoryId == Id && x.IsDelete==false);
        }

        public IQueryable<Article> GetArticles()
        {
            return _articleRepository.ListQueryable().Where(x=>x.IsDelete==false);
        }

        public Article GetArticlesById(int Id)
        {
            return _articleRepository.Find(x=>x.Id==Id && x.IsDelete==false);
        }

        public int Insert(Article article)
        {
            return _articleRepository.Insert(article);
        }

        public int Update(Article article)
        {
            return _articleRepository.Update(article);
        }

        public int Delete(Article article)
        {
            article.IsDelete = true;
            return _articleRepository.Update(article);
        }
    }
}
