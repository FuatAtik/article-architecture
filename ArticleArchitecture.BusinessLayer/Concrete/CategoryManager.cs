﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.DataAccessLayer.EntityFramework;
using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private readonly Repository<Category> _categoryRepository;

        public CategoryManager(Repository<Category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public List<Category> GetCategories()
        {
            return _categoryRepository.List(x=>x.IsDelete==false);
        }

        public Category GetCategoryById(int id)
        {
            return _categoryRepository.Find(x=>x.Id==id && x.IsDelete==false);
        }

        public int Insert(Category category)
        {
            return _categoryRepository.Insert(category);
        }

        public int Update(Category category)
        {
            return _categoryRepository.Update(category);
        }

        public int Delete(Category category)
        {
            category.IsDelete = true;
            return _categoryRepository.Update(category);
        }
    }
}
