﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.DataAccessLayer.EntityFramework;
using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Concrete
{
    public class CommentManager : ICommentService
    {
        public Repository<Comment> _commentRepository;

        public CommentManager(Repository<Comment> commentRepository)
        {
            _commentRepository = commentRepository;
        }
        public IQueryable<Comment> GetAllComment()
        {
            return _commentRepository.ListQueryable();
        }

        public Comment GetCommentById(int id)
        {
            return _commentRepository.Find(x => x.Id == id && x.IsDelete==false);
        }

        public int Update(Comment comment)
        {
            return _commentRepository.Update(comment);
        }

        public int Delete(Comment comment)
        {
            return _commentRepository.Delete(comment);
        }

        public int Insert(Comment comment)
        {
            return _commentRepository.Insert(comment);
        }
    }
}
