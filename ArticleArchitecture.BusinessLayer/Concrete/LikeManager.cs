﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.DataAccessLayer.EntityFramework;
using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Concrete
{
    public class LikeManager : ILikeService
    {
        public Repository<Liked> _likedRepository;

        public LikeManager(Repository<Liked> likedRepository)
        {
            _likedRepository = likedRepository;
        }

        public int Delete(Liked liked)
        {
            liked.IsDelete = true;
            return _likedRepository.Update(liked);
        }

        public List<Liked> GetAllLike()
        {
            return _likedRepository.List();
        }

        public IQueryable<Liked> GetAllLikeArticle()
        {
            return _likedRepository.ListQueryable();
        }

        public int Insert(Liked liked)
        {
            return _likedRepository.Insert(liked);
        }
    }
}
