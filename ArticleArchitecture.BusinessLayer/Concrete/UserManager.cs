﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.BusinessLayer.Results;
using ArticleArchitecture.Common.Helpers;
using ArticleArchitecture.DataAccessLayer.EntityFramework;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Entities.Messages;
using ArticleArchitecture.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.BusinessLayer.Concrete
{
    public class UserManager : IUserService
    {
        private readonly Repository<User> _userRepository;
        private readonly BusinessLayerResult<User> _businessLayerResult;

        public UserManager(Repository<User> userRepository, BusinessLayerResult<User> businessLayerResult)
        {
            _userRepository = userRepository;
            _businessLayerResult = businessLayerResult;
        }

        public BusinessLayerResult<User> Login(LoginViewModel loginViewModel)
        {
            var user = _userRepository.Find(x =>
                x.Username == loginViewModel.Username && x.Password == loginViewModel.Password);
            _businessLayerResult.Result = user;
            if (user != null)
            {
                if (!user.IsActive)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UserIsNotActive,
                        "Kullanıcı aktifleştirilmemiştir.");
                    _businessLayerResult.AddError(ErrorMessageCode.CheckYourEmail,
                        "Lütfen e-posta adresinizi kontrol ediniz.");
                }

                if (user.IsDelete)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UserIsDelete, "Bu kullanıcı silinmiştir.");
                    _businessLayerResult.AddError(ErrorMessageCode.TryAnotherUsername,
                        "Lütfen başka kullanıcı adı giriniz.");
                }
            }
            else
            {
                _businessLayerResult.AddError(ErrorMessageCode.UsernameOrPassWrong,
                    "Kullanıcı adı yada şifre uyuşmuyor.");
            }


            return _businessLayerResult;
        }

        public BusinessLayerResult<User> Register(RegisterViewModel registerViewModel)
        {
            // Kullanıcı username kontrolü..
            // Kullanıcı e-posta kontrolü..
            // Kayıt işlemi..
            // Aktivasyon e-postası gönderimi.
            var user = _userRepository.Find(x =>
                x.Username == registerViewModel.Username || x.Email == registerViewModel.Email);


            if (user != null)
            {
                if (user.Username == registerViewModel.Username)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (user.Email == registerViewModel.Email)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }
            }
            else
            {
                int dbResult = _userRepository.Insert(new User()
                {
                    Username = registerViewModel.Username,
                    Email = registerViewModel.Email,
                    // ProfileImageFile = "user-default.png",
                    Password = registerViewModel.Password,
                    ActivateGuid = Guid.NewGuid(),
                    IsActive = false,
                    IsAdmin = false
                });

                if (dbResult > 0)
                {
                    _businessLayerResult.Result = _userRepository.Find(x =>
                        x.Email == registerViewModel.Email && x.Username == registerViewModel.Username);

                    string siteUri = ConfigHelper.Get<string>("SiteRootUrl");
                    string activateUri = $"{siteUri}/Login/UserActivate/{_businessLayerResult.Result.ActivateGuid}";
                    string body =
                        $"Merhaba {_businessLayerResult.Result.Username};<br><br>Hesabınızı aktifleştirmek için <a href='{activateUri}' target='_blank'>tıklayınız</a>.";

                    MailHelper.SendMail(body, _businessLayerResult.Result.Email, "Atic Articles Hesap Aktifleştirme");
                }
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> ActivateUser(Guid activateId)
        {
            _businessLayerResult.Result = _userRepository.Find(x => x.ActivateGuid == activateId);

            if (_businessLayerResult.Result != null)
            {
                if (_businessLayerResult.Result.IsActive)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UserAlreadyActive,
                        "Kullanıcı zaten aktif edilmiştir.");
                    return _businessLayerResult;
                }

                _businessLayerResult.Result.IsActive = true;
                _userRepository.Update(_businessLayerResult.Result);
            }
            else
            {
                _businessLayerResult.AddError(ErrorMessageCode.ActivateIdDoesNotExists,
                    "Aktifleştirilecek kullanıcı bulunamadı.");
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> GetUserById(int id)
        {
            _businessLayerResult.Result = _userRepository.Find(x => x.Id == id && x.IsDelete == false);

            if (_businessLayerResult.Result == null)
            {
                _businessLayerResult.AddError(ErrorMessageCode.UserNotFound, "Kullanıcı bulunamadı.");
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> UpdateProfile(User data)
        {
            var user = _userRepository.Find(x =>
                x.Id != data.Id && (x.Username == data.Username || x.Email == data.Email));

            if (user != null && user.Id != data.Id)
            {
                if (user.Username == data.Username)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (user.Email == data.Email)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }

                return _businessLayerResult;
            }

            _businessLayerResult.Result = _userRepository.Find(x => x.Id == data.Id);
            _businessLayerResult.Result.Email = data.Email;
            _businessLayerResult.Result.Name = data.Name;
            _businessLayerResult.Result.Surname = data.Surname;
            _businessLayerResult.Result.Password = data.Password;
            _businessLayerResult.Result.Username = data.Username;

            if (data.ProfileImageFile != null)
            {
                _businessLayerResult.Result.ProfileImageFile = data.ProfileImageFile;
            }


            if (_userRepository.Update(_businessLayerResult.Result) == 0)
            {
                _businessLayerResult.AddError(ErrorMessageCode.ProfileCouldNotUpdated, "Profil güncellenemedi.");
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> RemoveUserById(int id)
        {
            var user = _userRepository.Find(x => x.Id == id);

            if (user != null)
            {
                user.IsDelete = true;
                if (_userRepository.Update(user) == 0)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UserCouldNotRemove, "Kullanıcı silinemedi.");
                    return _businessLayerResult;
                }
            }
            else
            {
                _businessLayerResult.AddError(ErrorMessageCode.UserCouldNotFind, "Kullanıcı bulunamadı.");
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> Insert(User data)
        {
            var user = _userRepository.Find(x => x.Username == data.Username || x.Email == data.Email);

            _businessLayerResult.Result = data;

            if (user != null)
            {
                if (user.Username == data.Username)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (user.Email == data.Email)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }
            }
            else
            {
                // _businessLayerResult.Result.ProfileImageFile = "user_boy.png";
                _businessLayerResult.Result.ActivateGuid = Guid.NewGuid();

                if (_userRepository.Insert(_businessLayerResult.Result) == 0)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UserCouldNotInserted, "Kullanıcı eklenemedi.");
                }
            }

            return _businessLayerResult;
        }

        public BusinessLayerResult<User> Update(User data)
        {
            var user = _userRepository.Find(x => x.Username == data.Username || x.Email == data.Email);
            _businessLayerResult.Result = data;

            if (user != null && user.Id != data.Id)
            {
                if (user.Username == data.Username)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı adı kayıtlı.");
                }

                if (user.Email == data.Email)
                {
                    _businessLayerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-posta adresi kayıtlı.");
                }

                return _businessLayerResult;
            }

            _businessLayerResult.Result = _userRepository.Find(x => x.Id == data.Id);
            _businessLayerResult.Result.Email = data.Email;
            _businessLayerResult.Result.Name = data.Name;
            _businessLayerResult.Result.Surname = data.Surname;
            _businessLayerResult.Result.Password = data.Password;
            _businessLayerResult.Result.Username = data.Username;
            _businessLayerResult.Result.IsActive = data.IsActive;
            _businessLayerResult.Result.IsAdmin = data.IsAdmin;

            if (_userRepository.Update(_businessLayerResult.Result) == 0)
            {
                _businessLayerResult.AddError(ErrorMessageCode.UserCouldNotUpdated, "Kullanıcı güncellenemedi.");
            }

            return _businessLayerResult;
        }

        public List<User> GetAllUser()
        {
            return _userRepository.List(x => x.IsDelete == false);
        }
    }
}