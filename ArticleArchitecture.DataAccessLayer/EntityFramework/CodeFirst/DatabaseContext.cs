﻿using System.Data.Entity;
using ArticleArchitecture.Entities.Entities;

namespace ArticleArchitecture.DataAccessLayer.EntityFramework
{
    public class DatabaseContext:DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Article> Article { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Liked> Liked { get; set; }
    }
}
