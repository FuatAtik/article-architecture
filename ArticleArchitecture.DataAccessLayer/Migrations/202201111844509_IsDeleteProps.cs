﻿namespace ArticleArchitecture.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsDeleteProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Article", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Article", "IsDelete", c => c.Boolean(nullable: false));
            AddColumn("dbo.Comment", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Comment", "IsDelete", c => c.Boolean(nullable: false));
            AddColumn("dbo.User", "IsDelete", c => c.Boolean(nullable: false));
            AddColumn("dbo.Liked", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Liked", "IsDelete");
            DropColumn("dbo.User", "IsDelete");
            DropColumn("dbo.Comment", "IsDelete");
            DropColumn("dbo.Comment", "IsActive");
            DropColumn("dbo.Article", "IsDelete");
            DropColumn("dbo.Article", "IsActive");
        }
    }
}
