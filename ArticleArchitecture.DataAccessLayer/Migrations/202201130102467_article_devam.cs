﻿namespace ArticleArchitecture.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class article_devam : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Article", "Title", c => c.String(nullable: false, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Article", "Title", c => c.String(nullable: false, maxLength: 60));
        }
    }
}
