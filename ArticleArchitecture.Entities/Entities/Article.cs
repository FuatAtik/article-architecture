﻿using ArticleArchitecture.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ArticleArchitecture.Entities.Entities
{
    [Table("Article")]
    public class Article : EntityBase
    {
        [DisplayName("Makale Başlığı"), Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Title { get; set; }

        [DisplayName("Makale Metni"), Required]
        [Column(TypeName = "varchar(MAX)")]
        [AllowHtml]
        public string Text { get; set; }

        [DisplayName("Makale Thumbnail")]
        [StringLength(30), ScaffoldColumn(false)]
        public string ImageFilename { get; set; }

        [DisplayName("Taslak")]
        public bool IsDraft { get; set; }

        [DisplayName("Beğenilme")]
        public int LikeCount { get; set; }

        public bool IsActive{ get; set; }
        public bool IsDelete{ get; set; }
        [DisplayName("Kategori")]
        public int CategoryId { get; set; }

        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual List<Liked> Likes { get; set; }


        public Article()
        {
            Comments = new List<Comment>();
            Likes = new List<Liked>();
        }
    }
}
