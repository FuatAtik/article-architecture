﻿using ArticleArchitecture.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.Entities.Entities
{
    [Table("Comment")]
    public class Comment : EntityBase
    {
        [Required, StringLength(300)]
        public string Text { get; set; }
      
        public bool IsActive{ get; set; }
        public bool IsDelete{ get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
    }
}
