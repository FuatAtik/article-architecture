﻿using ArticleArchitecture.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleArchitecture.Entities.Entities
{
    [Table("Liked")]
    public class Liked: EntityBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool IsDelete{ get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
    }
}
