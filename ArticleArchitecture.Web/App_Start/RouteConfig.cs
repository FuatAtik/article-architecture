﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ArticleArchitecture.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            //Article Routes
            routes.MapRoute("CategoryList", "category-list",
                new { controller = "Category", action = "Index" });

            routes.MapRoute("CategoryAdd", "new-category",
                new { controller = "Category", action = "Create" });

            routes.MapRoute("CategoryEdit", "category-edit/{id}",
                new { controller = "Category", action = "Edit", id = UrlParameter.Optional });

            routes.MapRoute("CategoryDetail", "category-detail/{id}",
                new { controller = "Category", action = "Detail", id = UrlParameter.Optional });

            routes.MapRoute("CategoryDelete", "category-delete/{id}",
                new { controller = "Category", action = "Delete", id = UrlParameter.Optional });


            //Article Routes
            routes.MapRoute("MyLikedArticles", "my-liked-articles",
                new { controller = "Article", action = "MyLikedArticles" });

            routes.MapRoute("ArticleEdit", "article-edit/{id}",
                new { controller = "Article", action = "Edit", id = UrlParameter.Optional });

            routes.MapRoute("ArticleDetail", "article-detail/{id}",
                new { controller = "Article", action = "Detail", id = UrlParameter.Optional });

            routes.MapRoute("ArticleDelete", "article-delete/{id}",
                new { controller = "Article", action = "Delete", id = UrlParameter.Optional });

            routes.MapRoute("NewArticle", "new-article",
                new { controller = "Article", action = "Create" });

            routes.MapRoute("MyArticle", "my-article",
                new { controller = "Article", action = "Index" });


            //User Routes
            routes.MapRoute("EditProfile", "editprofile",
                new { controller = "User", action = "EditProfile" });
            
            routes.MapRoute("ShowProfile", "showprofile",
                new { controller = "User", action = "Index" });

            routes.MapRoute("UserCreate", "new-user",
                new { controller = "User", action = "Create" });

            routes.MapRoute("UserList", "user-list",
                new { controller = "User", action = "Users" });

            routes.MapRoute("UserEdit", "user-edit/{id}",
              new { controller = "User", action = "Edit", id = UrlParameter.Optional });

            routes.MapRoute("UserDetail", "user-detail/{id}",
                new { controller = "User", action = "Detail", id = UrlParameter.Optional });

            routes.MapRoute("UserDelete", "user-delete/{id}",
                new { controller = "User", action = "Delete", id = UrlParameter.Optional });

            routes.MapRoute("AccessDenied", "AccessDenied",
                new { controller = "Login", action = "AccessDenied" });



            //Login Routes
            routes.MapRoute("Login", "login",
                new { controller = "Login", action = "Index" });

            routes.MapRoute("Register", "register",
                 new { controller = "Login", action = "Register" });

            routes.MapRoute("Logout", "logout",
                 new { controller = "Login", action = "Logout" });

          
            //HomePage Routes
            routes.MapRoute("About", "about",
                 new { controller = "Home", action = "About" });

            routes.MapRoute("Favorites", "favorites",
                 new { controller = "Home", action = "Favorites" });

            routes.MapRoute("SelectCategory", "category/{id}",
                 new { controller = "Home", action = "ByCategory", id = UrlParameter.Optional });


            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
          
          

        }
    }
}
