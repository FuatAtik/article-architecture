﻿$(function() {

    $('#modal_articledetail').on('show.bs.modal', function (e) {

        var btn = $(e.relatedTarget);
        articleId = btn.data("article-id");

        $("#modal_articledetail_body").load("/Article/GetNoteText/" + articleId);
    });

});