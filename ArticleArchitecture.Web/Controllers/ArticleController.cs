﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Web.Filters;
using ArticleArchitecture.Web.Models;

namespace ArticleArchitecture.Web.Controllers
{
    [Exc]
    public class ArticleController : Controller
    {
        private readonly IArticleService _articleManager;
        private readonly ICategoryService _categoryManager;
        private readonly ILikeService _LikeManager;

        public ArticleController(
            IArticleService articleManager,
            ILikeService LikeManager,
            ICategoryService categoryManager)
        {
            _articleManager = articleManager;
            _LikeManager = LikeManager;
            _categoryManager = categoryManager;
        }

        [Auth]
        public ActionResult Index()
        {
            List<Article> articles;
            if (!CurrentSession.User.IsAdmin)
            {
                articles = _articleManager.GetArticles().Include("Category").Include("User")
                    .Where(x => x.User.Id == CurrentSession.User.Id && x.IsDelete==false).OrderByDescending(x => x.ModifiedOn).ToList();
            }
            else
            {
                articles = _articleManager.GetArticles().ToList();
            }

            return View(articles);
        }

        [Auth]
        public ActionResult MyLikedArticles()
        {
            List<Article> articles = _LikeManager.GetAllLikeArticle().Include("Liked").Include("Article").Where(
                    x => x.User.Id == CurrentSession.User.Id).Select(x => x.Article).Include("Category").Include("User")
                .OrderByDescending(
                    x => x.ModifiedOn).ToList();

            return View(articles);
        }

        [Auth]
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Article article = _articleManager.GetArticlesById(id.Value);
            if (article == null)
            {
                return HttpNotFound();
            }

            return View(article);
        }

        [Auth]
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_categoryManager.GetCategories(), "Id", "Title");
            return View();
        }

        [Auth]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Article article, HttpPostedFileBase ImageFilename)
        {
            ModelState.Remove("CreatedOn");
            ModelState.Remove("ModifiedOn");
            ModelState.Remove("ModifiedUsername");

            if (ModelState.IsValid)
            {
                if (ImageFilename != null &&
                    (ImageFilename.ContentType == "image/jpeg" ||
                     ImageFilename.ContentType == "image/jpg" ||
                     ImageFilename.ContentType == "image/png"))
                {
                    string filename = $"article_{article.Id}.{ImageFilename.ContentType.Split('/')[1]}";

                    ImageFilename.SaveAs(Server.MapPath($"~/assets/images/article/{filename}"));
                    article.ImageFilename = filename;
                }
                
                article.User = CurrentSession.User;
                _articleManager.Insert(article);
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(_categoryManager.GetCategories(), "Id", "Title", article.CategoryId);
            return View(article);
        }

        [Auth]
        public ActionResult Edit(int? id)
        {
            Article checkArticle = _articleManager.GetArticles()
                .Where(x => x.Id == id.Value && x.ModifiedUsername == CurrentSession.User.Username).SingleOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!CurrentSession.User.IsAdmin)
            {
                if (checkArticle == null)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
            }


            Article article = _articleManager.GetArticlesById(id.Value);
            if (article == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryId = new SelectList(_categoryManager.GetCategories(), "Id", "Title", article.CategoryId);
            return View(article);
        }

        [Auth]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Article article, HttpPostedFileBase ImageFilename)
        {
            ModelState.Remove("CreatedOn");
            ModelState.Remove("ModifiedOn");
            ModelState.Remove("ModifiedUsername");
            if (ModelState.IsValid)
            {
                Article db_article = _articleManager.GetArticlesById(article.Id);

                if (ImageFilename != null &&
                    (ImageFilename.ContentType == "image/jpeg" ||
                     ImageFilename.ContentType == "image/jpg" ||
                     ImageFilename.ContentType == "image/png"))
                {
                    string filename = $"article_{article.Id}.{ImageFilename.ContentType.Split('/')[1]}";

                    ImageFilename.SaveAs(Server.MapPath($"~/assets/images/article/{filename}"));
                    db_article.ImageFilename = filename;
                }
                
                db_article.IsDraft = article.IsDraft;
                db_article.CategoryId = article.CategoryId;
                db_article.Text = article.Text;
                db_article.Title = article.Title;

                _articleManager.Update(db_article);
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(_categoryManager.GetCategories(), "Id", "Title", article.CategoryId);
            return View(article);
        }

        [Auth]
        public ActionResult Delete(int? id)
        {
            Article checkArticle = _articleManager.GetArticles()
                .Where(x => x.Id == id.Value && x.ModifiedUsername == CurrentSession.User.Username).SingleOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!CurrentSession.User.IsAdmin)
            {
                if (checkArticle == null)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
            }

            Article article = _articleManager.GetArticlesById(id.Value);
            if (article == null)
            {
                return HttpNotFound();
            }

            return View(article);
        }

        [Auth]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Article article = _articleManager.GetArticlesById(id);
            _articleManager.Delete(article);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetLiked(int[] ids)
        {
            if (CurrentSession.User != null)
            {
                int userid = CurrentSession.User.Id;
                List<int> likedArticleIds = new List<int>();

                if (ids != null)
                {
                    likedArticleIds = _LikeManager.GetAllLike().Where(
                        x => x.User.Id == userid && ids.Contains(x.Article.Id)).Select(
                        x => x.Article.Id).ToList();
                }
                else
                {
                    likedArticleIds = _LikeManager.GetAllLike().Where(
                        x => x.User.Id == userid).Select(
                        x => x.Article.Id).ToList();
                }

                return Json(new {result = likedArticleIds});
            }
            else
            {
                return Json(new {result = new List<int>()});
            }
        }

        [HttpPost]
        public ActionResult SetLikeState(int articleId, bool liked)
        {
            int result = 0;

            if (CurrentSession.User == null)
                return Json(new
                    {hasError = true, errorMessage = "Beğenme işlemi için giriş yapmalısınız.", result = 0});

            Liked like = _LikeManager.GetAllLike()
                .Find(x => x.Article.Id == articleId && x.User.Id == CurrentSession.User.Id);

            Article article = _articleManager.GetArticlesById(articleId);

            if (like != null && liked == false)
            {
                result = _LikeManager.Delete(like);
            }
            else if (like == null && liked == true)
            {
                result = _LikeManager.Insert(new Liked()
                {
                    User = CurrentSession.User,
                    Article = article
                });
            }

            if (result > 0)
            {
                if (liked)
                {
                    article.LikeCount++;
                }
                else
                {
                    article.LikeCount--;
                }

                result = _articleManager.Update(article);

                return Json(new {hasError = false, errorMessage = string.Empty, result = article.LikeCount});
            }

            return Json(new
                {hasError = true, errorMessage = "Beğenme işlemi gerçekleştirilemedi.", result = article.LikeCount});
        }


        public ActionResult GetNoteText(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Article article = _articleManager.GetArticlesById(id.Value);

            if (article == null)
            {
                return HttpNotFound();
            }

            return PartialView("_PartialArticleText", article);
        }
    }
}