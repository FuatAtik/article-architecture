﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Web.Filters;
using ArticleArchitecture.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ArticleArchitecture.Web.Controllers
{
    [Exc]
    public class CommentController : Controller
    {
        private readonly IArticleService _articleManager;
        private readonly ICommentService _commentManager;

        public CommentController(IArticleService articleManager, ICommentService commentManager)
        {
            _articleManager = articleManager;
            _commentManager = commentManager;
        }

        public ActionResult ShowArticleComments(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = _articleManager.GetArticles().Include("Comments").FirstOrDefault(x => x.Id == id);

            if (article == null)
            {
                return HttpNotFound();
            }

            return PartialView("_PartialComments", article.Comments);
        }

        [Auth]
        [HttpPost]
        public ActionResult Edit(int? id, string text)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Comment comment = _commentManager.GetCommentById(id.Value);

            if (comment == null)
            {
                return new HttpNotFoundResult();
            }

            comment.Text = text;

            if (_commentManager.Update(comment) > 0)
            {
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        [Auth]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Comment comment = _commentManager.GetCommentById(id.Value);

            if (comment == null)
            {
                return new HttpNotFoundResult();
            }

            if (_commentManager.Delete(comment) > 0)
            {
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        [Auth]
        [HttpPost]
        public ActionResult Create(Comment comment, int? articleId)
        {
            ModelState.Remove("CreatedOn");
            ModelState.Remove("ModifiedOn");
            ModelState.Remove("ModifiedUsername");

            if (ModelState.IsValid)
            {
                if (articleId == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                Article article= _articleManager.GetArticlesById(articleId.Value);

                if (article == null)
                {
                    return new HttpNotFoundResult();
                }

                comment.Article = article;
                comment.User = CurrentSession.User;

                if (_commentManager.Insert(comment) > 0)
                {
                    return Json(new { result = true }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }
    }
}