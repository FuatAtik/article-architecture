﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.BusinessLayer.Concrete;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ArticleArchitecture.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryService _categoryManager;
        private readonly IArticleService _articleManager;
        private readonly HomePageViewModel _homePageViewModel;
        
        public HomeController(
            IArticleService articleManager,
            ICategoryService categoryManager,
            HomePageViewModel homePageModel)
        {
            _categoryManager = categoryManager;
            _articleManager = articleManager;
            _homePageViewModel = homePageModel;
        }
        public ActionResult Index()
        {
            _homePageViewModel.Articles = _articleManager.GetArticles().Where(x => x.IsDraft == false).OrderByDescending(x=>x.ModifiedOn).ToList();
            _homePageViewModel.Categories = _categoryManager.GetCategories().ToList();

            return View(_homePageViewModel);
        }
        public ActionResult ByCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Article> articles = _articleManager.GetArticlesByCategoryId(id.Value).Where(x => x.IsDraft == false).OrderByDescending(x => x.ModifiedOn).ToList();
            _homePageViewModel.Articles = articles;
            _homePageViewModel.Categories = _categoryManager.GetCategories().ToList();
            return View("Index", _homePageViewModel);
        }
        public ActionResult Favorites()
        {
            _homePageViewModel.Articles = _articleManager.GetArticles().Where(x => x.IsDraft == false).OrderByDescending(x => x.LikeCount).ToList();
            _homePageViewModel.Categories = _categoryManager.GetCategories().ToList();
            return View("Index",_homePageViewModel);
        }
        public ActionResult About()
        {
            _homePageViewModel.Categories = _categoryManager.GetCategories().ToList();
            return View(_homePageViewModel);
        }
    }
}