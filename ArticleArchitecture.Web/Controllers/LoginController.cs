﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.BusinessLayer.Results;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Entities.ViewModel;
using ArticleArchitecture.Web.Models;
using ArticleArchitecture.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArticleArchitecture.Web.Controllers
{
    public class LoginController : Controller
    {

        private readonly BusinessLayerResult<User> _businessLayerResult;
        private readonly IUserService _userManager;


        public LoginController(
            BusinessLayerResult<User> businessLayerResult,
            IUserService userManager)
        {
            _businessLayerResult = businessLayerResult;
            _userManager = userManager;
        }



        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("Index")]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loginOperaiton = _userManager.Login(model);

                if (loginOperaiton.Errors.Count > 0)
                {
                    loginOperaiton.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }

                CurrentSession.Set<User>("login", loginOperaiton.Result); 
                return RedirectToAction("/","Home");
            }

            return View(model);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var registerOperaition = _userManager.Register(model);

                if (registerOperaition.Errors.Count > 0)
                {
                    registerOperaition.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }

                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Kayıt Başarılı",
                    RedirectingUrl = "/login",
                };

                notifyObj.Items.Add("Lütfen e-posta adresinize gönderdiğimiz aktivasyon link'ine tıklayarak hesabınızı aktive ediniz. Hesabınızı aktive etmeden not ekleyemez ve beğenme yapamazsınız.");

                return View("Ok", notifyObj);
            }

            return View(model);
        }

        public ActionResult UserActivate(Guid id)
        {
            var result = _userManager.ActivateUser(id);

            if (result.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Geçersiz İşlem",
                    Items = _businessLayerResult.Errors
                };

                return View("Error", errorNotifyObj);
            }

            OkViewModel okNotifyObj = new OkViewModel()
            {
                Title = "Hesap Aktifleştirildi",
                RedirectingUrl = "/Login"
            };

            okNotifyObj.Items.Add("Hesabınız aktifleştirildi. Artık not paylaşabilir ve beğenme yapabilirsiniz.");

            return View("Ok", okNotifyObj);
        }

        public ActionResult Logout()
        {
            CurrentSession.Clear();
            return RedirectToAction("", "Home");
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult NoAccessPermission()
        {
            return View();
        }

        public ActionResult HasError()
        {
            return View();
        }
    }
}