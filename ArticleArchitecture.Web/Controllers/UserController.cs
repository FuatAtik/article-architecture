using System.Net;
using System.Web;
using System.Web.Mvc;
using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Web.Filters;
using ArticleArchitecture.Web.Models;
using ArticleArchitecture.Web.Models.ViewModels;

namespace ArticleArchitecture.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userManager;

        public UserController(IUserService userManager)
        {
            _userManager = userManager;
        }

        [Auth]
        public ActionResult Index()
        {
            var currentUser = _userManager.GetUserById(CurrentSession.User.Id);
            if (currentUser.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Hata Oluştu",
                    Items = currentUser.Errors
                };

                return View("Error", errorNotifyObj);
            }

            return View(currentUser.Result);
        }

        [Auth]
        public ActionResult EditProfile()
        {
            var currentUser = _userManager.GetUserById(CurrentSession.User.Id);

            if (currentUser.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Hata Oluştu",
                    Items = currentUser.Errors
                };

                return View("Error", errorNotifyObj);
            }

            return View(currentUser.Result);
        }

        [Auth]
        [HttpPost]
        public ActionResult EditProfile(User model, HttpPostedFileBase ProfileImage)
        {
            ModelState.Remove("ModifiedUsername");

            if (ModelState.IsValid)
            {
                //if (ProfileImage != null &&
                //    (ProfileImage.ContentType == "image/jpeg" ||
                //    ProfileImage.ContentType == "image/jpg" ||
                //    ProfileImage.ContentType == "image/png"))
                //{
                //    string filename = $"user_{model.Id}.{ProfileImage.ContentType.Split('/')[1]}";

                //    ProfileImage.SaveAs(Server.MapPath($"~/assets/images/users/{filename}"));
                //    model.ProfileImageFilename = filename;
                //}

                if (ProfileImage != null)
                {
                    model.ProfileImageFile = new byte[ProfileImage.ContentLength];
                    ProfileImage.InputStream.Read(model.ProfileImageFile, 0, ProfileImage.ContentLength);
                }
               


                var updateUser = _userManager.UpdateProfile(model);

                if (updateUser.Errors.Count > 0)
                {
                    ErrorViewModel errorNotifyObj = new ErrorViewModel()
                    {
                        Items = updateUser.Errors,
                        Title = "Profil Güncellenemedi.",
                        RedirectingUrl = "/User/EditProfile"
                    };

                    return View("Error", errorNotifyObj);
                }

                // Profil güncellendiği için session güncellendi.
                CurrentSession.Set<User>("login", updateUser.Result);

                return RedirectToAction("", "User");
            }

            return View(model);
        }

        [Auth]
        public ActionResult DeleteProfile()
        {
            var user = _userManager.RemoveUserById(CurrentSession.User.Id);

            if (user.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Items = user.Errors,
                    Title = "Profil Silinemedi.",
                    RedirectingUrl = "/Home/ShowProfile"
                };

                return View("Error", errorNotifyObj);
            }

            Session.Clear();

            return RedirectToAction("Index", "Home");
        }


        [Auth]
        [AuthAdmin]
        [Exc]
        public ActionResult Users()
        {
            return View(_userManager.GetAllUser());
        }

        [Auth]
        [AuthAdmin]
        [Exc]
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _userManager.GetUserById(id.Value).Result;

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [Auth]
        [AuthAdmin]
        [Exc]
        public ActionResult Create()
        {
            return View();
        }


        [Auth]
        [AuthAdmin]
        [Exc]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            ModelState.Remove("CreatedOn");
            ModelState.Remove("ModifiedOn");
            ModelState.Remove("ModifiedUsername");

            if (ModelState.IsValid)
            {
                var result = _userManager.Insert(user);

                if (result.Errors.Count > 0)
                {
                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(user);
                }

                return RedirectToAction("/", "user-list");
            }

            return View(user);
        }

        [Auth]
        [AuthAdmin]
        [Exc]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _userManager.GetUserById(id.Value).Result;

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }


        [Auth]
        [AuthAdmin]
        [Exc]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            ModelState.Remove("CreatedOn");
            ModelState.Remove("ModifiedOn");
            ModelState.Remove("ModifiedUsername");

            if (ModelState.IsValid)
            {
                var result = _userManager.Update(user);

                if (result.Errors.Count > 0)
                {
                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(user);
                }

                return RedirectToAction("/", "user-list");
            }

            return View(user);
        }


        [Auth]
        [AuthAdmin]
        [Exc]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _userManager.GetUserById(id.Value).Result;

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [Auth]
        [AuthAdmin]
        [Exc]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            User user = _userManager.GetUserById(id).Result;
            _userManager.RemoveUserById(user.Id);

            return RedirectToAction("/", "user-list");
        }
    }
}