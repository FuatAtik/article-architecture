﻿using ArticleArchitecture.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArticleArchitecture.Web.Filters
{
    public class AuthActive : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (CurrentSession.User != null && CurrentSession.User.IsActive == false)
            {
                filterContext.Result = new RedirectResult("/Login/AccessDenied");
            }
        }
    }
}