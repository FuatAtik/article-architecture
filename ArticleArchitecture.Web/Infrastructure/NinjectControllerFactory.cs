﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.BusinessLayer.Concrete;
using ArticleArchitecture.Entities.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ArticleArchitecture.Web.Infrastructure
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        public IKernel _ninjectKernel;

        public NinjectControllerFactory()
        {
            _ninjectKernel = new StandardKernel();
            AddBllBindings();
        }
        private void AddBllBindings()
        {
            _ninjectKernel.Bind<ICategoryService>().To<CategoryManager>()
                .WithConstructorArgument("Category", new Category());

            _ninjectKernel.Bind<IArticleService>().To<ArticleManager>()
               .WithConstructorArgument("Article", new Article());

            _ninjectKernel.Bind<IUserService>().To<UserManager>()
                 .WithConstructorArgument("User", new User());

            _ninjectKernel.Bind<ILikeService>().To<LikeManager>()
                 .WithConstructorArgument("Liked", new Liked());

            _ninjectKernel.Bind<ICommentService>().To<CommentManager>()
                 .WithConstructorArgument("Comment", new Comment());
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)_ninjectKernel.Get(controllerType);
        }
    }
}