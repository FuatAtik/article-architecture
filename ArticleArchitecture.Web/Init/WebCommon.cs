﻿using ArticleArchitecture.Common.Abstract;
using ArticleArchitecture.Entities.Entities;
using ArticleArchitecture.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleArchitecture.Web.Init
{
    public class WebCommon: ICommon
    {
        public string GetCurrentUsername()
        {
            User user = CurrentSession.User;

            if (user != null)
                return user.Username;
            else
                return "system";
        }
    }
}