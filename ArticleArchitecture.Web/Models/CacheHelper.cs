﻿using ArticleArchitecture.BusinessLayer.Abstract;
using ArticleArchitecture.BusinessLayer.Concrete;
using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace ArticleArchitecture.Web.Models
{
    public class CacheHelper
    {
        private readonly ICategoryService _categoryManager;

        public CacheHelper(ICategoryService categoryManager)
        {
            _categoryManager = categoryManager;
        }

        public List<Category> GetCategoriesFromCache()
        {
            var result = WebCache.Get("category-cache");

            if (result == null)
            {
                result = _categoryManager.GetCategories();

                WebCache.Set("category-cache", result, 20, true);
            }

            return result;
        }

        public void RemoveCategoriesFromCache()
        {
            Remove("category-cache");
        }

        public void Remove(string key)
        {
            WebCache.Remove(key);
        }
    }
}