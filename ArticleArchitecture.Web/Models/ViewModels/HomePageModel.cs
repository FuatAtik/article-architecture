﻿using ArticleArchitecture.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleArchitecture.Web.Models
{
    public class HomePageViewModel
    {
        public List<Category> Categories { get; set; }
        public List<Article> Articles { get; set; }
    }
}